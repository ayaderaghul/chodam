class AddPictureToPostonces < ActiveRecord::Migration[5.1]
  def change
    add_column :postonces, :picture, :string
  end
end
