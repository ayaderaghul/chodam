class CreatePostonces < ActiveRecord::Migration[5.1]
  def change
    create_table :postonces do |t|
      t.text :content

      t.timestamps
    end
  end
end
