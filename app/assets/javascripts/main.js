let stickIt = (w, navstr, d) => {
    let e = document.querySelector(navstr);
    if (w.scrollY >= d) {
	e.classList.add("sticky")
    } else {
	e.classList.remove("sticky")
    }
}
let toggleButton = (w, buttonid) => {
    let b = document.querySelector(buttonid)
    if (w.scrollY > 20) {
	b.style.display = "block";
    } else {
	b.style.display = "none";
    }
}
let getPosition = (navstr) => {
    return document.querySelector(navstr).offsetTop;
}

document.addEventListener("DOMContentLoaded", function(){   
    let navbarposition = getPosition("nav");

    window.onresize = function(){
	document.querySelector("nav").classList.remove("sticky");
	navbarposition = getPosition("nav");

    }

    window.onscroll = function() {

	stickIt(window, "nav", navbarposition);
	toggleButton(window, "#toTop");	
	document.querySelector("#toTop").addEventListener("click", function() {
	    window.scrollTo(0,0);
	    stickIt(window, "nav", navbarposition);
	})
    }
})

	 
	 



