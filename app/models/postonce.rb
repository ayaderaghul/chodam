class Postonce < ApplicationRecord
	validates :content, presence: true, length: { maximum: 140 }
	default_scope -> { order(created_at: :desc) }
	mount_uploader :picture, PictureUploader
end
