class ProductsController < ApplicationController
	before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user, only: :destroy

  def create
    @product = current_user.products.build(product_params)
    if @product.save
      flash[:success] = "Đã đăng sản phẩm!"
      redirect_to current_user
    else
      @feed_items = []
      flash[:danger] = "Chưa được. Bà con đã điền thông tin và chọn hình chưa?"
      redirect_to current_user
    end
  end

  def destroy
    @product.destroy
    flash[:success] = "Đã xóa sản phẩm."
    redirect_to request.referrer || root_url
  end

  private

    def product_params
      params.require(:product).permit(:content, :picture)
    end
    def correct_user
      @product = current_user.products.find_by(id: params[:id])
      redirect_to root_url if @product.nil?
    end
end
