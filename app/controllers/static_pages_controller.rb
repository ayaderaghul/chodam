class StaticPagesController < ApplicationController
  def home
  	if logged_in?
  		@feed_items = current_user.feed.all
  		@feed_news = current_user.feed.all
  	end
  	
  	@postonces = Postonce.all
  	@postonce = Postonce.new
  	@microposts = Micropost.all
    @micropost = Micropost.new
    
    @comment = Comment.new
  end
  def help
  end
  
end
