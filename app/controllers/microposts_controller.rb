class MicropostsController < ApplicationController
	before_action :logged_in_user, only: [:create, :destroy]
	before_action :correct_user, only: :destroy
	def show 
		if Micropost.find_by(id: params[:id])
			@micropost = Micropost.find_by(id: params[:id])
			@comments = @micropost.comments.all 
			@comment = @micropost.comments.build
		end

	end

	def create
		@micropost = current_user.microposts.build(micropost_params)
		if @micropost.save
			flash[:success] = "Đã đăng tản văn."
			redirect_to current_user
		else
			flash[:danger] = "Chưa được."
			@feed_news = []
			redirect_to current_user
		end

	end
	def destroy
		@micropost.destroy
		flash[:success] = "Đã xóa tản văn."
		redirect_to request.referrer || root_url
	end
	private
	def micropost_params
		params.require(:micropost).permit(:content, :picture, :header)
	end
	def correct_user
		@micropost = current_user.microposts.find_by(id: params[:id])
		redirect_to root_url if @micropost.nil?
	end
end


