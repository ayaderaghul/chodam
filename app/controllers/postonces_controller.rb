class PostoncesController < ApplicationController
	def create
		@postonce = Postonce.new(postonce_params)
		if @postonce.save
			flash[:success] = "Đã đăng sản phẩm."
			redirect_to root_url
		else
			flash.now[:danger] = 'Chưa đăng được sản phẩm. Bà con đã viết tin và chọn ảnh chưa?'
			render 'static_pages/home'
		end
	end
	def destroy 
		@postonce = Postonce.find_by(id: params[:format])
		@postonce.destroy
    flash[:success] = "Đã xóa sản phẩm."
    redirect_to request.referrer || root_url
	end
	private
	def postonce_params
		params.require(:postonce).permit(:content, :picture)
	end
end
