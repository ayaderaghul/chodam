class CommentsController < ApplicationController
	before_action :logged_in_user, only: [:create, :destroy]
	before_action :correct_user, only: :destroy
	def create
		@micropost = Micropost.find_by(id: params[:micropost_id])
		@comment = @micropost.comments.create(comment_params)
		@comment.user_id = current_user.id
		if @comment.save
			flash[:success] = "Đã đăng bình luận!"
			redirect_to root_url
		else
			flash.now[:danger] = "Lỗi."
			render 'static_pages/home'
		end
	end
	def destroy
		@comment.destroy
		flash[:success] = "Đã xóa bình luận."
		redirect_to request.referrer || root_url

	end
	private
	def comment_params
		params.require(:comment).permit(:content, :user_id, :micropost_id)
	end
	def correct_user
		@comment = current_user.comments.find_by(id: params[:id])
		redirect_to root_url if @comment.nil?
	end
end
