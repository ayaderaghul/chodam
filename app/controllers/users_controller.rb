class UsersController < ApplicationController
	before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
	before_action :correct_user, only: [:edit, :update]
	before_action :admin_user, only: :destroy
def index
	@users = User.all
end
	def show

		@user = User.find(params[:id])
		@products = @user.products.all
		@product = @user.products.build
		@microposts = @user.microposts.all
		@micropost = @user.microposts.build
		@comments = @micropost.comments.all
		@comment = @micropost.comments.build
	end
	def new
		@user = User.new
	end
	def create
		@user = User.new(user_params)
		if @user.save
			log_in @user
			flash[:success] = "Chào #{ @user.name }, chúc hắn sức khỏe!"
			redirect_to @user
		else
			render 'new'
		end
	end
	def edit
		#@user = User.find(params[:id])
	end
	def update
		@user = User.find(params[:id])
		if @user.update_attributes(user_params)
			flash[:success] = "Đã cập nhật!"
			redirect_to @user
		else
			render 'show'
		end
	end
	def destroy
    User.find(params[:id]).destroy
    flash[:success] = "Đã xóa."
    redirect_to users_url
  end
  def following
    	@title = "Following"
    	@user = User.find(params[:id])
    	@users = @user.following.all 
    	render 'show_follow'
    end
    def followers
    	@title = "Followers"
    	@user = User.find(params[:id])
    	@users = @user.followers.all 
    	render 'show_follow'
    end
	private
	def user_params
		params.require(:user).permit(:name, :email, :password, :password_confirmation, :picture)
	end
	
	def correct_user
		@user = User.find(params[:id])
		redirect_to(root_url) unless current_user?(@user)
	end
	def admin_user
      redirect_to(root_url) unless current_user.admin?
    end

end
