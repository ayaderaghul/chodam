Rails.application.routes.draw do
  root 'static_pages#home'
  get '/help', to: 'static_pages#help'

  get '/signup', to: 'users#new'
  post '/signup', to: 'users#create'
  patch '/signup', to: 'users#update'

  get '/login', to: 'sessions#new'
  post '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'

  post '/postonce', to: 'postonces#create'
  delete "/postonce#{/\d+/}", to: 'postonces#destroy'
 
  post '/comments', to: 'comments#create'
  delete '/comments', to: 'comments#destroy'
  resources :users do 
  	member do 
  		get :following, :followers 
  	end
  end
  resources :products, only: [:create, :destroy]
  resources :relationships, only: [:create, :destroy]
  resources :postonces, only: [:create, :destroy]
  resources :microposts, only: [:create, :destroy]
  resources :comments, only: [:create, :destroy]
end
